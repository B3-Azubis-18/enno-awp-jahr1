
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
using namespace std;

int main() {

	// Variablen
	double degree = 0.0;
	double radian = 0.0;

	// Eingabe
	cout << "Bitte geben Sie das Gradma� ein: ";
	cin >> degree;
	cout << endl;

	// Verarbeitung
	radian = degree * (M_PI / 180);

	// Ausgabe
	cout << "Das Bogenma� ist: " << radian;

	getchar();
	getchar();

	return 0;
}