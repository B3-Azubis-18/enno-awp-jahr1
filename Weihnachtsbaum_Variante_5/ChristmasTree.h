#pragma once
#ifndef ChristmasTree_H
#define ChristmasTree_H
#include<vector>
#include"Point.h"

class ChristmasTree
{
public:
	ChristmasTree(int, int);
	~ChristmasTree();
	void buildTree();
	std::vector<std::vector<Point>> getTree();
	void printTree();

private:
	ChristmasTree();
	std::vector<std::vector<Point>> tree;
	int height;
	bool calculated;
};

#endif