#pragma once
#ifndef Point_H
#define Point_H

// Colors in the Windows-Console:
// https://www.daniweb.com/programming/software-development/code/216345/add-a-little-color-to-your-console-text
class Point
{
public:
	Point();
	Point(int, int);
	~Point();
	void setColor(int, int);
	void setCharacter(char);
	int getX();
	int getY();
	int getColor();
	char getCharacter();
private:
	int Foreground;
	int Background;
	int x;
	int y;
	char character;
};

#endif