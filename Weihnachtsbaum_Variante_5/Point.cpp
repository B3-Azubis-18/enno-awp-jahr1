#include "Point.h"

Point::Point()
{
	this->x = 0;
	this->y = 0;
	this->Background = 0;
	this->Foreground = 0;
}

Point::Point(int x, int y)
{
	this->x = x;
	this->y = y;
	this->Background = 0;
	this->Foreground = 15;
}

Point::~Point()
{
	//
}

void Point::setColor(int foreground, int background)
{
	this->Background = background;
	this->Foreground = foreground;
}

void Point::setCharacter(char c)
{
	this->character = c;
}

int Point::getX()
{
	return this->x;
}

int Point::getY()
{
	return this->y;
}

// Color-Codes: https://ss64.com/nt/color.html
int Point::getColor() {
	return this->Foreground + this->Background * 16;
}

char Point::getCharacter()
{
	return this->character;
}
