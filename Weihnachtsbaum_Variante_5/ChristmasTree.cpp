#include"ChristmasTree.h"
#include<iostream>
#include <windows.h>

// Seperation between code and header: https://stackoverflow.com/q/9579930/4730773

ChristmasTree::ChristmasTree()
{
	throw new std::logic_error("Du kannst keinen Baum ohne Hoehe und Breite erstellen!");
}

ChristmasTree::ChristmasTree(int height, int width)
{
	this->height = height;
	tree.resize(height + 2);
	for (int i = 0; i < height + 2; i++) {
		tree[i].resize(width);
	}
	this->calculated = false;
}

ChristmasTree::~ChristmasTree()
{
}

void ChristmasTree::buildTree()
{
	int centre = this->height;
	for (int currentRow = 1; currentRow <= height + 2; currentRow++) {
		for (int currentColumn = 1; currentColumn < height * 2; currentColumn++) {
			// Centre || Left || Right
			Point p{ currentColumn, currentRow };
			if (currentRow > height && currentColumn == centre) {
				p.setCharacter('H');
				p.setColor(6, 0);
			}
			else if ((currentColumn == centre
				|| (currentColumn < centre && centre - currentRow < currentColumn)
				|| (currentColumn > centre && centre + currentRow > currentColumn)) && currentRow <= height)
			{
				p.setCharacter('x');
				p.setColor(2, 0);
			}
			else
			{
				p.setCharacter(' ');
			}
			tree[currentRow - 1][currentColumn - 1] = p;
		}
	}
	this->calculated = true;
}

void ChristmasTree::printTree() {
	if (!calculated) {
		std::cout << "Baum noch nicht berechnet!";
	}
	else
	{
		HANDLE hConsole;
		hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		// For-Schleife mit Console Handle f�r Farben.

		for (std::vector<Point> row : tree)
		{
			for (Point p : row)
			{
				SetConsoleTextAttribute(hConsole, p.getColor());
				std::cout << p.getCharacter();
			}
			std::cout << std::endl;
		}
		SetConsoleTextAttribute(hConsole, 15);
	}
}

std::vector<std::vector<Point>> ChristmasTree::getTree()
{
	if (calculated) {
		return tree;
	}
	return std::vector<std::vector<Point>>();
}
