#include <iostream>

using namespace std;

int main() {
	double Endkapital;
	int Anfangskapital;
	float Zinssatz;
	int Laufzeit;

	//Eingabe
	cout << "Anfangskapital: ";
	cin >> Anfangskapital;
	cout << endl;

	cout << "Zinssatz: ";
	cin >> Zinssatz;
	cout << endl;

	cout << "Laufzeit: ";
	cin >> Laufzeit;
	cout << endl;

	//Verarbeitung
	Endkapital = Anfangskapital * pow((1 + Zinssatz / 100), Laufzeit);

	//Ausgabe
	cout << "Endkapital: " << Endkapital;

	getchar();
	getchar();
	return 0;
}