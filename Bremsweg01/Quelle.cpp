#include <iostream>;
#include <cmath>

using namespace std;

int main() {

	// Variable 
	float f_Anhaltweg = 0;
	float f_Geschwindigkeit = 0;
	int i_Reaktion = 0;
	int i_Verzoegerung = 0;
	char c_Nochmal = ' ';
	int i_Verhaeltnisse = 0;
	float f_Abstand = 0;
	do {
		// Variable z�rucksetzen 
		f_Anhaltweg = 0;
		f_Geschwindigkeit = 0;
		i_Reaktion = 0;
		i_Verzoegerung = 0;
		c_Nochmal = ' ';
		i_Verhaeltnisse = 0;
		f_Abstand = 0;
		// Bildschirm l�chen 
		system("cls");

		cout << "-- Bestimmung des Anhaltewegs -- " << endl << endl;
		cout << "Bitte geben Sie die gefahrende Geschwindigkeit in Km/h ein: ";
		cin >> f_Geschwindigkeit;
		cout << endl << "Bitte geben Sie die restliche Strecke bis zum Hindernis in m ein: ";
		cin >> f_Abstand;
		cout << endl << "Bitte geben Sie die Reaktionszeit in s (Schrecksekunde ca.1) ein: ";
		cin >> i_Reaktion;
		cout << endl << "Bitte geben Sie die den Verzoegerungswert in m/s2 an (9 , 7 , 5 oder 3): ";
		cin >> i_Verzoegerung;
		f_Geschwindigkeit = f_Geschwindigkeit / 3.6;
		f_Anhaltweg = (f_Geschwindigkeit * i_Reaktion) + (pow(f_Geschwindigkeit,2) / (2 * i_Verzoegerung));
		cout << endl << "Restlicher Weg bis zum Hindernis: " << f_Abstand;
		cout << endl << "Benoetigten Anhaltsweg: " << f_Anhaltweg;
		if (f_Anhaltweg < f_Abstand) {
			cout << endl << endl << "Glueck gehabt!";
		}
		else {
			cout << endl << endl << "Es kam zum Crash!";
		}
		cout << endl << endl << "Wollen Sie es noch eine Berechnung durchf�ren? (J/j)";
		cin >> c_Nochmal;

	} while (c_Nochmal == 'j' || c_Nochmal == 'J');


	return 0;
}