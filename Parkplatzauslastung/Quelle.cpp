#include<iostream>
#include<Windows.h>

int parkplatz[50] = { 0 };

void parkplatz_reservieren();
void parkplatz_belegt();
void parkplatz_belegen();

int main() {
	SetConsoleOutputCP(CP_UTF8);

	int eingabe;
	bool erneut = true;

	while (erneut) {
		eingabe = -1;
		std::cout << "Willkommen auf unserem Parkplatz! Wollen sie einen Parkplatz:" << std::endl;
		std::cout << "\r1. reservieren oder" << std::endl;
		std::cout << "\r2. belegen oder" << std::endl;
		std::cout << u8"\r3. den Status des Parkplatzes �berpr�fen oder" << std::endl;
		std::cout << "\r4. das Programm beenden?" << std::endl;
		std::cin >> eingabe;

		switch (eingabe)
		{
		case 1:
			parkplatz_reservieren();
			break;
		case 2:
			parkplatz_belegen();
			break;
		case 3:
			parkplatz_belegt();
			break;
		case 4:
			erneut = false;
			std::cout << "Auf Wiedersehen!" << std::endl;
			break;
		default:
			std::cout << "Eingabe nicht verstanden. Programm startet von vorne!" << std::endl;
			break;
		}
	}

	std::getchar();
	std::getchar();
	return  0;
}

void parkplatz_reservieren() {
	int parkplatznr = -1;

	std::cout << u8"Welchen Parkplatz m�chten sie reservieren: ";
	std::cin >> parkplatznr;
	if (parkplatznr > 50 || parkplatznr < 1) {
		std::cout << "Diese Parkplatznummer existiert nicht!" << std::endl;
	}
	else
	{
		if (parkplatz[parkplatznr] == 0) {
			parkplatz[parkplatznr - 1] = 2;
			std::cout << "Der Parkplatz " << parkplatznr << u8" wurde f�r Sie reserviert!" << std::endl;
		}
		else
		{
			std::cout << "Der Parkplatz " << parkplatznr << "ist bereits belegt oder reserviert!" << std::endl;
		}
	}
}

void parkplatz_belegen() {
	int parkplatznr = -1;

	std::cout << u8"Welchen Parkplatz m�chten sie belegen: ";
	std::cin >> parkplatznr;
	if (parkplatznr > 50 || parkplatznr < 1) {
		std::cout << "Diese Parkplatznummer existiert nicht!" << std::endl;
	}
	else
	{
		if (parkplatz[parkplatznr] == 0) {
			parkplatz[parkplatznr - 1] = 2;
			std::cout << "Der Parkplatz " << parkplatznr << u8" wurde f�r Sie belegt!" << std::endl;
		}
		else
		{
			std::cout << "Der Parkplatz " << parkplatznr << "ist bereits belegt oder reserviert!" << std::endl;
		}
	}
}

void parkplatz_belegt() {
	std::cout << "Der Parkplatzstatus ist: " << std::endl;
	for (int i = 0; i < 50; i++) {
		if (parkplatz[i] == 1) {
			std::cout << "Platz " << (i + 1) << " ist belegt." << std::endl;
		}
		else
		{
			if (parkplatz[i] == 2) {
				std::cout << "Platz " << (i+1) << " ist reserviert." << std::endl;
			}
			else
			{
				std::cout << "Platz " << (i + 1) << " ist frei." << std::endl;
			}
		}
	}
}
