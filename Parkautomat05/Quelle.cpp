#include <iostream>
#include <Windows.h>

namespace constants {
	const int takt = 15;
	const float einheit = 0.5;
}

int main() {
	SetConsoleOutputCP(CP_UTF8);

	int zeit;
	float betrag;
	char taste;

	do {
		system("cls"),
		zeit = 0;
		betrag = 0.0;
		taste = ' ';
		std::cout << "p oder P dr�cken" << std::endl;

		do
		{
			std::cout << u8"Bitte p/P zum erh�hen dr�cken, oder jede andere Taste zum abbrechen: ";
			std::cin >> taste;

			if (taste == 'p' || taste == 'P') {
				zeit += constants::takt;
				betrag = constants::einheit * zeit / constants::takt;
			}
		} while ((taste == 'p' || taste == 'P') && zeit < 45);

		std::cout << std::endl;
		if (zeit > 0) {
			std::cout << "Parkzeit: " << zeit << std::endl;
			std::cout << "Zahlbetrag: " << betrag << std::endl;
		}
		else
		{
			std::cout << "Ein leerer Parkschein wird nicht gedruckt." << std::endl;
		}
		system("pause");
	} while (true);

	getchar();
	getchar();
	return 0;
}