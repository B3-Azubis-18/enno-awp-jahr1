#include <iostream>;

// WARNING: This algorithm starts counting at the intuitive starting number one, not the usual number zero like
//          elsewhere in computer science!
int main() {
	unsigned int height = -1;
	char input = ' ';
	bool again = true;

	std::cout << "- Variante 3 -" << std::endl;
	do {
		std::cout << "Hoehe des Baumes eingeben <5-40>: ";
		std::cin >> height;
		std::cout << std::endl;

		// Max Width of the level: currentRow*2-1
		// Max Width of the tree: height*2-1
		// Centre of the level: height-1
		// To the right or left side of the centre are currentRow "x".

		for (int currentRow = 1; currentRow <= height; currentRow++) {
			for (int currentColumn = 1; currentColumn < height*2; currentColumn++) {
				// Centre || Left || Right
				int centre = height;
				if (currentColumn == centre 
					|| (currentColumn < centre && centre - currentRow < currentColumn) 
					|| (currentColumn > centre && centre + currentRow > currentColumn))
				{
					std::cout << "x";
				}
				else
				{
					std::cout << " ";
				}
			}
			std::cout << std::endl;
		}

		do {
			std::cout << std::endl;
			std::cout << "Noch ein Baum? <j/n>: ";
			std::cin >> input;
			if (input == 'j' || input == 'J') {
				break;
			}
			else if (input == 'n' || input == 'N')
			{
				again = false;
				break;
			}
			else
			{
				std::cout << "Falsche Eingabe! Bitte versuche es erneut!";
			}
		} while (true);
	} while (again);

	return 0;
}