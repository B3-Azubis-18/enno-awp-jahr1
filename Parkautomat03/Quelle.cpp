#include <iostream>
#include <Windows.h>
#include <stdio.h>

char cTaste = 'a';

void einlesen()
{
	// Eingabe
	std::cout << "Eingabe: ";
	std::cin >> cTaste;
	std::cout << std::endl;
}

int main()
{
	SetConsoleOutputCP(CP_UTF8);
	//setvbuf(stdout, nullptr, _IOFBF, 1000);

	// Variablen
	int iZeit = 0;
	float fBetrag = 0.0;
	const int iTakt = 15;
	const float fEinheit = 0.5;

	// Anleitung
	std::cout << "Willkommen bei Parkautomat V01.0" << std::endl;
	std::cout << u8"M�chten sie ein Parkticket f�r 15min, so dr�cken sie die Taste 'p', wenn sie eine andere Taste dr�cken beendet sich das Programm." << std::endl;

	einlesen();

	// Verarbeitung
	if (cTaste == 'p')
	{
		iZeit = iZeit + iTakt;
		fBetrag = fEinheit * iZeit / iTakt;

		einlesen();

		if (cTaste == 'p')
		{
			iZeit = iZeit + iTakt;
			fBetrag = fEinheit * iZeit / iTakt;

			einlesen();

			if (cTaste == 'o')
			{
				// Ausgabe
				std::cout << u8"Sie d�rfen f�r: " << iZeit << "min parken und haben " << fBetrag << " gezahlt." << std::endl;
			}
			else
			{
				// Ausgabe
				std::cout << u8"max. 30 min." << std::endl;
			}
		}
		else
		{
			// Ausgabe
			std::cout << u8"Sie d�rfen f�r: " << iZeit << "min parken und haben " << fBetrag << " gezahlt." << std::endl;
		}
	}
	else
	{
		// Ausgabe
		std::cout << u8"Kein Parkticket gedruckt.";
	}

	getchar();
	getchar();
	return 0;
}
