#include <iostream>;
#include <cmath>
#include <Windows.h>

int main() {
	// FIXME: Overflow of odd if till is even.
	SetConsoleOutputCP(CP_UTF8);

	double n = 0;
	double x = 0;
	unsigned int sum = 0;

	std::cout << u8"Bitte gewŁnschtes n eingeben: ";
	std::cin >> n;
	std::cout << u8"Bitte gewŁnschtes x eingeben: ";
	std::cin >> x;
	
	for (int i = 0; i <= n;i++) {
		std::cout << x << "^" << i << " Ergebnis: " << pow(x, i) << std::endl;
		sum += pow(x, i);
	}

	std::cout << "Ergebnis: " << sum << std::endl;
	std::getchar();
	std::getchar();

	return 0;
}